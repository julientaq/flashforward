---
layout: home.njk
class: home
title: Flash Forward
subtitle: A Shuttleworth Foundation Celebration
baseline: Power is not what you gain, it’s what you give.
---


Launched in 2011, Flash Grants support a global, decentralised network of individuals transforming generosity for social change. Now, it’s time to learn their stories.

FlashForward will connect our community to celebrate their work and create new conduits for shared solutions and resilient modes of support. The event will be curated by grantees to enable a proliferation in forms of knowledge exchange around the world. 

Join us for one week as we converge to demonstrate a truth that underpins Flash Grants:
