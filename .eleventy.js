let markdownIt = require('markdown-it');
let implicitFigures = require('markdown-it-implicit-figures');
let blockEmbedPlugin = require('markdown-it-block-embed');
let html5Embed = require('markdown-it-html5-embed');
let frame = require('markdown-it-iframe');




module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy({"static/css":"css"});
  eleventyConfig.addPassthroughCopy({"static/js":"js"});
  eleventyConfig.addPassthroughCopy({"static/admin":"admin"});
  eleventyConfig.addPassthroughCopy({"static/images":"images"});
  eleventyConfig.addPassthroughCopy({"static/font":"font"});

  

  // packages for markdown
  let markdownLib = markdownIt({html: true}).use(implicitFigures, {
    dataType: false, // <figure data-type="image">, default: false
    figcaption: false, // <figcaption>alternative text</figcaption>, default: false
    tabindex: true, // <figure tabindex="1+n">..., default: false
    link: true // <a href="img.png"><img src="img.png"></a>, default: false
  }).use(frame).use(blockEmbedPlugin, {
    containerClassName: "video-embed",
    outputPlayerSize: false
  }).use(html5Embed, {
    html5embed: {
      useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
      useLinkSyntax: true // Enables video/audio embed with []() syntax
    }
  });
  eleventyConfig.setLibrary("md", markdownLib);

  eleventyConfig.addFilter('markdownify', value => markdownLib.render(value));



  return {
    dir: {
      input: "views",
      output: "public",
      includes: "_includes"
    },
  }

};  